#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"
#include "JoesTime.h"

const char *ssid = "****";
const char *password = "****";
const int timeZoneOffsetSeconds = 36000; // +10:00 AEST in seconds.

MDNSResponder mdns;
ESP8266WebServer server(80);
Time myTime;

typedef class pinSetting_t
{
  public:
    int On;
    int Off;
};

#define intMax 2147483647
#define pin_array_length 6
int pin[] = { 2, 16, 15, 14, 13, 12 };
pinSetting_t pinSetting[pin_array_length];
long nextToggle[pin_array_length];
int currentState[pin_array_length];

void SetConfigToDefault()
{
  int now = millis();

  for (int i; i < pin_array_length; i++)
  {
    pinMode(pin[i], OUTPUT);
    pinSetting[i].On = 300000;
    pinSetting[i].Off = 300000;
    nextToggle[i] = now;
    currentState[i] = LOW;
  }
}

void setup(void) {   
  Serial.begin(115200);
  ConnectToWifi();
  SetupWebServer();
  
  myTime.SetTimeFromNtp();
  myTime.SetTimeZone(timeZoneOffsetSeconds);

  EEPROM.begin(512);
  ReadEep();
  
  if (pinSetting[0].On == -1 && pinSetting[0].Off == -1)
  {
    SetConfigToDefault();  
  }

  digitalWrite(LED_BUILTIN, 0);
}

void ConnectToWifi()
{
  WiFi.hostname("esp8266-02");
  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN, 1);
    delay(250);
    Serial.print(".");
    digitalWrite(LED_BUILTIN, 0);
  }

  Serial.println("");
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void SetupWebServer()
{
  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/setup", HandleSetupRequest);

  server.on("/save", HandleSaveRequest);

  server.on("/read", HandleReadRequest);

  server.on("/currentState", HandleCurrentStateRequest);

  server.on("/factoryReset", HandleFactoryResetRequest);

  server.on("/pause", HandlePauseRequest);

  server.on("/extend", HandleExtendRequest);

  server.on("/resume", HandleResumeRequest);

  server.on("/time", HandleGetTimeRequest);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

String GetPinsHtmlDropdown()
{
  String result = "<select name='pin'>";
  for (int i; i < pin_array_length; i++)
  {
    result += "<option value='" + String(pin[i]) + "'>" +  String(pin[i]) + "</option>";
  }
  result += "</select>";
  return result;
}

void handleRoot() {
  String html = "<b>Set the timer for a specified pin:</b><br />"
                "pin = The pin to be setup <i>(required)</i><br />"
                "on = The duration of the on cycle in milliseconds <i>(optional)</i><br />"
                "of = The duration of the off cycle in milliseconds <i>(optional)</i><br />"
                "<i>/setup?pin=3&on=1000&off=1000</i><br />"
                "<form action='/setup' method='GET'>"
                "Pin: " + GetPinsHtmlDropdown() +
                "On: <input name='on' value='1000' size='10' /> "
                "Off: <input name='off' value='1000' size='10' /> "
                "<input type='submit' value='Save' /> "
                "</form>"
                "<br />"
                "<b>Turn off a pin by skipping to the off cycle for the specified duration:</b><br />"
                "pin = The pin to turn off <i>(required)</i><br />"
                "duration = The duration of the off cycle in milliseconds <i>(required)</i><br />"
                "<i>/pause?pin=3&delay=1000</i><br />"
                "<form action='/pause' method='GET'>"
                "Pin: " + GetPinsHtmlDropdown() +
                "Duration: <input name='duration' value='1000' size='10' /> "
                "<input type='submit' value='Pause' /> "
                "</form>"
                "<br />"
                "<b>Turn on a pin by skipping to the on cycle for the specified duration:</b><br />"
                "pin = The pin to turn on <i>(required)</i><br />"
                "duration = The duration of the on cycle in milliseconds <i>(required)</i><br />"
                "</i>/extend?pin=3&delay=1000</i><br />"
                "<form action='/extend' method='GET'>"
                "Pin: " + GetPinsHtmlDropdown() +
                "Duration: <input name='duration' value='1000' size='10' /> "
                "<input type='submit' value='Extend' /> "
                "</form>"
                "<br />"
                "<b>Resume a pins regular cycle by skipping to the regular on cycle:</b><br />"
                "pin = The pin to be resumed <i>(required)</i><br />"
                "<i>/resume?pin=3</i><br />"
                "<form action='/resume' method='GET'>"
                "Pin: " + GetPinsHtmlDropdown() +
                "<input type='submit' value='Resume' /> "
                "</form>"
                "<br />"
                "<b>Save the current configuration to EEPROM</b><br />"
                "<i>/save</i><br />"
                "<form action='/save' method='GET'>"
                "<input type='submit' value='Save' /> "
                "</form>"
                "<br />"
                "<b>Read the configuration from EEPROM into current configuration</b><br />"
                "<i>/read</i><br />"
                "<form action='/read' method='GET'>"
                "<input type='submit' value='Read' /> "
                "</form>"
                "<br />"
                "<b>Display the current configuration and state of each pin</b><br />"
                "<i>/currentState</i><br />"
                "<form action='/currentState' method='GET'>"
                "<input type='submit' value='State' /> "
                "</form>"
                "<br />"
                "<b>Reset the current configuration to a default configuration</b><br />"
                "I'll level with you, This didn't really come from a factory.<br />"
                "<i>/factoryReset</i><br />"
                "<form action='/factoryReset' method='GET'>"
                "<input type='submit' value='Reset' /> "
                "</form>"  ;
  server.send(200, "text/html", html);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void loop(void) {
  server.handleClient();
  DoToggle();

  if (millis() % 360000 == 0)
  {
      myTime.SetTimeFromNtp();
  }
}

void DoToggle()
{
  for (int i; i < pin_array_length; i++)
  {
    if (millis() > nextToggle[i])
    {
      if (currentState[i] == HIGH)
      {
        currentState[i] = LOW;
        nextToggle[i] = millis() + pinSetting[i].Off;
      }
      else
      {
        currentState[i] = HIGH;
        nextToggle[i] = millis() + pinSetting[i].On;
      }
      digitalWrite(pin[i], currentState[i]);
      Serial.println("Pin: " + String(pin[i]) + " set to: " + String(currentState[i]));
    }
  }
}

void HandleCurrentStateRequest()
{
  String message = "{\n"
                   "  \"currentMillis\":" + String(millis()) + ",\n"
                   "  \"pins\":[\n";
  for (int i; i < pin_array_length; i++)
  {
    message += "    {\n"
               "      \"pinNumber\":" + String(pin[i]) + ",\n"
               "      \"nextToggle\":" + String(nextToggle[i]) + ",\n"
               "      \"currentState\":" + String(currentState[i]) + ",\n"
               "      \"onDelay\":" + String(pinSetting[i].On) + ",\n"
               "      \"offDelay\":" + String(pinSetting[i].Off) + "\n"
               "    }";

    if (i < pin_array_length - 1)
    {
      message += ",\n";
    }
  }

  message += "\n  ]\n}";
  server.send(200, "application/json", message);
}

void HandlePauseRequest()
{
  if (server.arg("pin") == "")
  {
    server.send(400, "application/json", GenerateMissingParameterJson("pin", "pin is mandatory"));
    return;
  }

  if (server.arg("duration") == "")
  {
    server.send(400, "application/json", GenerateMissingParameterJson("duration", "duration is mandatory"));
    return;
  }

  Serial.println("Request received to pause pin: " + server.arg("pin") + " for " + server.arg("duration"));

  int indexOfPin;
  bool foundPin = false;
  for (int i; i < pin_array_length; i++)
  {
    Serial.println(String(pin[i])); // for some reason this doesn't work if we don't print it.
    if (String(pin[i]) == server.arg("pin"))
    {
      indexOfPin = i;
      foundPin = true;
      break;
    }
  }

  if (foundPin != true)
  {
    server.send(400, "application/json", GenerateMissingParameterJson("pin", "pin supplied is not valid. " + GetValidPinsMessage()));
    return;
  }

  currentState[indexOfPin] = LOW;
  digitalWrite(pin[indexOfPin], LOW);
  nextToggle[indexOfPin] = millis() + server.arg("duration").toInt();
  Serial.println("Set next toggle to: " + String(nextToggle[indexOfPin]));

  String message = "{\n"
                   "  \"success\":true,\n"
                   "  \"pinNumber\":" + String(pin[indexOfPin]) + ",\n"
                   "  \"nextToggle\":" + String(nextToggle[indexOfPin]) + ",\n"
                   "  \"currentMillis\":" + String(millis()) + "\n"
                   "}";
  server.send(200, "application/json", message);
}

void HandleExtendRequest()
{
  if (server.arg("pin") == "")
  {
    server.send(400, "application/json", GenerateMissingParameterJson("pin", "pin is mandatory"));
    return;
  }

  if (server.arg("duration") == "")
  {
    server.send(400, "application/json", GenerateMissingParameterJson("duration", "duration is mandatory"));
    return;
  }

  Serial.println("Request received to prolong pin: " + server.arg("pin") + " for " + server.arg("duration"));

  int indexOfPin;
  bool foundPin = false;
  for (int i; i < pin_array_length; i++)
  {
    Serial.println(String(pin[i])); // for some reason this doesn't work if we don't print it.
    if (String(pin[i]) == server.arg("pin"))
    {
      indexOfPin = i;
      foundPin = true;
      break;
    }
  }

  if (foundPin != true)
  {
    server.send(400, "application/json", GenerateMissingParameterJson("pin", "pin supplied is not valid. " + GetValidPinsMessage()));
    return;
  }

  currentState[indexOfPin] = HIGH;
  digitalWrite(pin[indexOfPin], HIGH);
  nextToggle[indexOfPin] = millis() + server.arg("duration").toInt();
  Serial.println("Set next toggle to: " + String(nextToggle[indexOfPin]));

  String message = "{\n"
                   "  \"success\":true,\n"
                   "  \"pinNumber\":" + String(pin[indexOfPin]) + ",\n"
                   "  \"nextToggle\":" + String(nextToggle[indexOfPin]) + ",\n"
                   "  \"currentMillis\":" + String(millis()) + "\n"
                   "}";
  server.send(200, "application/json", message);
}

void HandleResumeRequest()
{
  if (server.arg("pin") == "")
  {
    server.send(400, "application/json", GenerateMissingParameterJson("pin", "pin is mandatory"));
    return;
  }

  int indexOfPin;
  bool foundPin = false;
  for (int i; i < pin_array_length; i++)
  {
    Serial.println(String(pin[i])); // for some reason this doesn't work if we don't print it.
    if (String(pin[i]) == server.arg("pin"))
    {
      indexOfPin = i;
      foundPin = true;
      break;
    }
  }

  if (foundPin != true)
  {
    server.send(400, "application/json", GenerateMissingParameterJson("pin", "pin supplied is not valid. " + GetValidPinsMessage()));
  }

  currentState[indexOfPin] = LOW;
  nextToggle[indexOfPin] = millis();

  String message = "{\n"
                   "  \"success\":true,\n"
                   "  \"pinNumber\":" + String(pin[indexOfPin]) + ",\n"
                   "  \"nextToggle\":" + String(nextToggle[indexOfPin]) + ",\n"
                   "  \"currentMillis\":" + String(nextToggle[indexOfPin]) + "\n"
                   "}";

  server.send(200, "application/json", message);
}

void HandleSetupRequest() {
  if (server.arg("pin") == "")
  {
    server.send(400, "application/json", GenerateMissingParameterJson("pin", "pin is mandatory"));
    return;
  }

  int indexOfPin;
  bool foundPin = false;
  for (int i; i < pin_array_length; i++)
  {
    Serial.println("query string pin paramter is: " + server.arg("pin") + ". loop pin is:" + String(pin[i]));
    if (String(pin[i]) == server.arg("pin"))
    {
      indexOfPin = i;
      foundPin = true;
      Serial.println("matched pin from query string");
      break;
    }
  }

  if (foundPin != true)
  {
    server.send(400, "application/json", GenerateMissingParameterJson("pin", "pin supplied is not valid. " + GetValidPinsMessage()));
    return;
  }

  String message = "{\n"
                   "  \"success\":true,\n"
                   "  \"pinNumber\":" + String(pin[indexOfPin]) + ",\n"
                   "  \"onPrevious\":" + String(pinSetting[indexOfPin].On) + ",\n";

  if (server.arg("on") != "")
  {
    pinSetting[indexOfPin].On = server.arg("on").toInt();
  }

  message += "  \"onCurrent\":" + String(pinSetting[indexOfPin].On) + ",\n"
             "  \"offPrevious\":" + String(pinSetting[indexOfPin].Off) + ",\n";

  if (server.arg("off") != "")
  {
    pinSetting[indexOfPin].Off = server.arg("off").toInt();
  }

  message += "  \"offCurrent\":" + String(pinSetting[indexOfPin].Off) + ",\n";

  nextToggle[indexOfPin] = millis();
  message += "  \"nextToggle\":" + String(nextToggle[indexOfPin]) + ",\n"
             "  \"currentMillis\":" + String(nextToggle[indexOfPin]) + "\n"
             "}";
  server.send(200, "application/json", message);
}

void HandleFactoryResetRequest()
{
  if (server.arg("sure") == "yes")
  {
    SetConfigToDefault();
    server.send(200, "text/plain", "Current settings have been reset to default. \n"
                "If you want to keep these settings please call /save\n"
                "You can still get your old settings back by restarting without calling /save\n"
                "I know we're not making this easy, but it's a feature not a bug :P"
               );
  }
  else
  {
    server.send(200, "text/plain", "This will clear all your setting and reset them back to default.\n"
                "Default setting will set each pin to cycle every 5 minutes.\n"
                "If you're sure you want to reset then call this method again with the below: \n"
                "/factoryReset?sure=yes"
               );
  }
}

void HandleSaveRequest()
{
  SaveRunningConfig();
  String message = "{\n"
                   "  \"success\":true\n"
                   "}";
  server.send(200, "application/json", message);
}

void HandleReadRequest()
{
  ReadEep();
  String message = "{\n"
                   "  \"success\":true\n"
                   "}";
  server.send(200, "application/json", message);
}

void SaveRunningConfig()
{
  EEPROM_writeAnything(0, pinSetting);
  EEPROM.commit();
  Serial.println("called write eeprom");
}

void ReadEep()
{
  EEPROM_readAnything(0, pinSetting);
  Serial.println("called eeprom_read_block");
}

String GetValidPinsMessage()
{
  String message = "Valid pins are: ";
  for (int i; i < pin_array_length; i++)
  {
    Serial.println(String(pin[i]));
    message += String(pin[i]);

    if (i < pin_array_length - 1)
    {
      message += ", ";
    }
  }

  return message;
}

String GenerateMissingParameterJson(String field, String message)
{
  return "{\n"
         "  \"success\":false,\n"
         "  \"errors\":[\n"
         "    {\n"
         "      \"parameter\":\"" + field + "\",\n"
         "      \"message\":\"" + message + "\"\n"
         "    }\n"
         "  ]\n"
         "}";
}

void HandleGetTimeRequest()
{
  Time_t now = myTime.Now();
  String msg = "{\n"
  "  epoch:" + String(now.Epoch) + ",\n"
  "  hour:" + String(now.Hour) + ",\n"   
  "  minute:" + String(now.Minute) + ",\n"
  "  second:" + String(now.Second) + ",\n"
  "  secondsSinceMidnight:" + String(now.SecondsSinceMidnight) + "\n"
  "}";
  server.send(200, "application/json", msg);
}

